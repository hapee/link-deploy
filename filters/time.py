import datetime
import datetime
import dateutil.parser
import sys

if sys.version_info[0] < 3:
    raise Exception("Must be using Python 3")

fmt = "%Y-%m-%dT%H:%M:%S.%fZ"


def add_time(dt, **kwargs):
    return dt + datetime.timedelta(**kwargs)


def local_time_iso8601(string, **kwargs):
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()


def add_time_iso8601(string, **kwargs):
    dt = dateutil.parser.parse(string)
    return (dt + datetime.timedelta(**kwargs)).strftime(fmt)


class FilterModule(object):
    def filters(self):
        return {
            "local_time_iso8601": local_time_iso8601,
            "add_time": add_time,
            "add_time_iso8601": add_time_iso8601,
        }
